from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from . import views


app_name = 'blogs'


urlpatterns = [
    path('', views.BlogsView.as_view(), name='blogs_list'),
    path('blog/<str:slug>/', views.BlogView.as_view(), name='items'),
    path('Categories/<str:slug>/', views.CategoryView.as_view(), name='category'),
    path('home_page', views.HomePageView.as_view(), name='home_page'),
    path('author/<str:name>/', views.AuthorView.as_view(), name='author_blogs'),
    path('user_home/', views.UserHomeView.as_view(), name='user_home'),
    path('add/', views.AddBlogFormView.as_view(), name='add_blog'),
    path('update/<str:slug>/', views.UpdateBlogFormView.as_view(), name='update_blog'),
    path('add_category/', views.AddCategoryFormView.as_view(), name='add_category')
]

