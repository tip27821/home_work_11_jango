from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.text import slugify


class Category(models.Model):
    category = models.CharField(max_length=30)
    slug = models.SlugField(unique=True, allow_unicode=True, default=None, blank=True, null=True)

    def __str__(self):
        return str(self.category)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.slug:
            self.slug = slugify(self.category)
        super().save(force_insert=False, force_update=False, using=None, update_fields=None)


class Blogs(models.Model):
    name = models.CharField(max_length=80)
    slug = models.SlugField(unique=True, allow_unicode=True, default=None, blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    text = models.TextField(blank=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, default=None, null=True)
    date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(default='blog.png', blank=True)

    def __str__(self):
        return f'{self.name} - {self.author} - {self.category} - {self.date}'

    def uniq_slug(self, slug):
        unique_slug = slug
        num = 1
        while Blogs.objects.filter(slug=unique_slug).exists():
            unique_slug = slug + f"_{num}"
            num+=1
        return unique_slug

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.slug:
            self.slug = self.uniq_slug(slugify(self.name))

        super().save(force_insert=False, force_update=False, using=None, update_fields=None)

    def get_absolute_url(self):
        return reverse('blogs:items', kwargs={'slug': self.slug})
