from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render,
from .models import Blogs, Category
from django.views.generic import ListView, DetailView, UpdateView, CreateView


class BlogsView(ListView):
    model = Blogs
    context_object_name = 'blogs'
    template_name = 'blogs/all_blogs.html'


class BlogView(DetailView):
    model = Blogs
    context_object_name = 'blog'
    template_name = 'blogs/blog.html'


class CategoryView(ListView):
    model = Blogs
    context_object_name = 'blogs'
    template_name = 'blogs/category.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['blogs'] = Blogs.objects.filter(category__slug=self.kwargs["slug"])
        context['category'] = Category.objects.get(slug=self.kwargs["slug"]).category
        return context


class AuthorView(ListView):
    model = Blogs
    context_object_name = 'blogs'
    template_name = 'blogs/author_blogs.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['blogs'] = Blogs.objects.filter(author__username=self.kwargs['name'])
        context['author'] = User.objects.get(username=self.kwargs['name']).first_name
        return context


class HomePageView(ListView):
    model = Blogs
    context_object_name = 'blogs'
    template_name = 'blogs/home_page.html'


class AddBlogFormView(LoginRequiredMixin, CreateView):
    template_name = 'blogs/add_blog.html'
    model = Blogs
    fields = ['name', 'category', 'text', 'image']

    def form_valid(self, form):
        new_blog = form.save(commit=False)
        new_blog.author = self.request.user
        return super().form_valid(form)


class UpdateBlogFormView(LoginRequiredMixin, UpdateView):
        template_name = 'blogs/update_blog.html'
        model = Blogs
        fields = ['name', 'category', 'text', 'image']

        def form_valid(self, form):
            if self.request.user.id == self.object.author.id:
                return super().form_valid(form)
            else:
                author = self.object.author
                return render(self.request, 'blogs/error.html', context={'author': author})


class UserHomeView(LoginRequiredMixin, ListView):
    template_name = 'blogs/user_home.html'
    model = Blogs

    def form_valid(self, form):
        new_blog = form.save(commit=False)
        new_blog.author = self.request.user
        return super().request


class AddCategoryFormView(LoginRequiredMixin, CreateView):
    template_name = 'blogs/add_category.html'
    model = Category
    fields = ['category']

    def form_valid(self, form):
        new_blog = form.save(commit=False)
        new_blog.author = self.request.user
        return super().form_valid(form)
