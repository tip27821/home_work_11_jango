FROM alpine:3.13

RUN apk update \
    && apk add --no-cache python3 py3-pip \
    && apk add jpeg-dev zlib-dev libjpeg gcc python3-dev musl-dev postgresql-dev


WORKDIR /opt/jango_api

COPY ./requirements.txt ./

RUN pip install -r requirements.txt

COPY ./ ./

ENV PYTHONPATH="/opt/jango_api"


ENTRYPOINT ["python3", "manage.py"]

EXPOSE 8000

CMD ["runserver", "0.0.0.0:8000"]
