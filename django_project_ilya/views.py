from django.shortcuts import render


def home_page(request):
    return render(request, 'blogs/home_page.html')
